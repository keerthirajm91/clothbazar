﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Entities
{
    public class Category : BaseEntity
    {
        //From Product Class, list of Products under a given category
        public List<Product> Products { get; set; }
    }
}
